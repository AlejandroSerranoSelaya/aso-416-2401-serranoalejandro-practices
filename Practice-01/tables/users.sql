CREATE TABLE users (
    user_id SERIAL,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    username_initial CHAR(1) NOT NULL,
    PRIMARY KEY (user_id, username_initial)
) PARTITION BY LIST (username_initial);

CREATE TABLE posts (
    post_id SERIAL,
    user_id INT,
    username_initial CHAR(1) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (post_id, created_at),
    FOREIGN KEY (user_id, username_initial) REFERENCES users (user_id, username_initial)
) PARTITION BY RANGE (created_at);

CREATE TABLE comments (
    comment_id SERIAL,
    post_id INT,
    user_id INT,
    username_initial CHAR(1) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (comment_id, post_id),
    FOREIGN KEY (post_id, created_at) REFERENCES posts (post_id, created_at),
    FOREIGN KEY (user_id, username_initial) REFERENCES users (user_id, username_initial)
) PARTITION BY HASH (post_id);