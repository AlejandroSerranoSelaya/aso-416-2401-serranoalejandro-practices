
# Configuración y Ejecución de MongoDB con Docker Compose

## Paso 1: Crear el archivo `init-mongo.js`

Crea un archivo llamado `init-mongo.js` en el directorio del proyecto. Este archivo se utilizará para inicializar la base de datos MongoDB con datos de ejemplo.

## Paso 2: Configurar Docker y crear el archivo `docker-compose.yml`

Crea un archivo llamado `docker-compose.yml` en el mismo directorio y agrega la configuración para MongoDB.

```yaml
version: '3.8'
services:
  mongo:
    image: mongo:latest
    container_name: mongo_container
    ports:
      - "27017:27017"
    volumes:
      - mongo-data:/data/db
      - ./init-mongo.js:/docker-entrypoint-initdb.d/init-mongo.js:ro

volumes:
  mongo-data:
```

## Paso 3: Ejecutar Docker Compose

En la terminal, navega al directorio donde están los archivos `init-mongo.js` y `docker-compose.yml`, y ejecuta el siguiente comando:

```sh
docker-compose up -d
```

Espera aproximadamente 20 segundos para que MongoDB se inicialice y los datos se carguen correctamente.


## Paso 4: Conectarse a MongoDB usando MongoDB Compass

Abre MongoDB Compass e ingresa el siguiente connection string para conectarte a la base de datos `compras`:

```
mongodb://localhost:27017/compras
```

![Paso 4](images/paso4.png)

## Paso 5: Crear y ejecutar el código no optimizado

Crea un archivo llamado `query_non_optimized.js` y agrega el siguiente código para realizar la consulta de todas las compras de todos los usuarios sin optimización:

```javascript
db = db.getSiblingDB("compras");

const usersCollection = db.getCollection('users');
const productsCollection = db.getCollection('products');

let allPurchases = [];

let startTime = Date.now();

usersCollection.find().forEach(user => {
    let userPurchases = {
        user_id: user._id,
        name: user.name,
        products: []
    };

    productsCollection.find({ user_id: user._id }).forEach(product => {
        userPurchases.products.push(product);
    });

    allPurchases.push(userPurchases);
});

let endTime = Date.now();
let executionTime = endTime - startTime;

printjson(allPurchases);
print(`Execution time: ${executionTime} ms`);
```

Ejecuta el script en la terminal con el siguiente comando:

```sh
mongo compras ./query_non_optimized.js
```

![Paso 5](images/paso5.png)

## Paso 6: Crear y ejecutar el código optimizado

Crea un archivo llamado `query_optimized.js` y agrega el siguiente código para realizar la consulta de todas las compras de todos los usuarios de manera optimizada:

```javascript
db = db.getSiblingDB("compras");

let startTime = Date.now();

const result = db.users.aggregate([
    {
        $lookup: {
            from: "products",
            localField: "_id",
            foreignField: "user_id",
            as: "products"
        }
    }
]).toArray();

let endTime = Date.now();
let executionTime = endTime - startTime;

printjson(result);
print(`Execution time: ${executionTime} ms`);
```

Ejecuta el script en la terminal con el siguiente comando:

```sh
mongo compras ./query_optimized.js
```

![Paso 6](./images/paso6.png)

## Paso 7: Comparar resultados

Compara los tiempos de ejecución impresos por ambos scripts (`query_non_optimized.js` y `query_optimized.js`). El script optimizado debe mostrar un tiempo de ejecución significativamente menor.

![Paso 7](./images/paso7.png)
![Paso 8](./images/paso8.png)
```