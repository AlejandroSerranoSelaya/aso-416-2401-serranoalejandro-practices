db = db.getSiblingDB("compras");

let startTime = Date.now();

const result = db.users.aggregate([
    {
        $lookup: {
            from: "products",
            localField: "_id",
            foreignField: "user_id",
            as: "products"
        }
    }
]).toArray();

let endTime = Date.now();
let executionTime = endTime - startTime;

printjson(result);
print(`Execution time: ${executionTime} ms`);
