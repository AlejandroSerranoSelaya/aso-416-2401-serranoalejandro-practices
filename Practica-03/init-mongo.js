db = db.getSiblingDB("compras")


db.createCollection('users');
db.createCollection('products');

const usersCollection = db.getCollection('users');
const productsCollection = db.getCollection('products');


for (let i = 1; i <= 1000; i++) {
    let user = {
        _id: i,
        name: `User_${i}`
    };

    usersCollection.insertOne(user);

    for (let j = 1; j <= 100; j++) {
        let product = {
            user_id: i,
            product_id: `${i}_${j}`,
            name: `Product_${j}`,
        };
        productsCollection.insertOne(product);
    }
}