db = db.getSiblingDB("compras");

const usersCollection = db.getCollection('users');
const productsCollection = db.getCollection('products');

let allPurchases = [];

let startTime = Date.now();

usersCollection.find().forEach(user => {
    let userPurchases = {
        user_id: user._id,
        name: user.name,
        products: []
    };

    productsCollection.find({ user_id: user._id }).forEach(product => {
        userPurchases.products.push(product);
    });

    allPurchases.push(userPurchases);
});

let endTime = Date.now();
let executionTime = endTime - startTime;

printjson(allPurchases);
print(`Execution time: ${executionTime} ms`);
