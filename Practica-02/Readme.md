# Project Practica-02: PostgreSQL Cluster with Docker Compose

This project sets up a PostgreSQL cluster using Docker Compose with three nodes: one master node and two replica nodes with data replication. Below are the steps to configure, run, and verify the cluster.

## Project Structure

```
Practica-02/
├── config/
│   ├── pg_hba.conf
│   ├── postgresql.conf
│   ├── postgresql2.conf
│   └── postgresql3.conf
├── images/
│   ├── .png
└── docker-compose.yml
```

## Step 1: Create the Project Structure

First, create the directory structure:

```bash
mkdir -p Practica-02/config
mkdir -p Practica-02/images
cd Practica-02
```

## Step 2: Create Configuration Files

### `postgresql.conf` for the Master Node

Create `config/postgresql.conf`:

![postgresql.conf](./images/postgresqlConfig.png)

### `postgresql2.conf` for Replica Node 1

Create `config/postgresql2.conf`:

![postgresql2.conf](./images/postgresqlConf2.png)

### `postgresql3.conf` for Replica Node 2

Create `config/postgresql3.conf`:

![postgresql3.conf](./images/postgresqlConf3.png)

### `pg_hba.conf` for All Nodes

Create `config/pg_hba.conf`:

![pg_hba.conf](./images/pg_hba.png)

## Step 3: Create the `docker-compose.yml` File

Create the `docker-compose.yml` file to define the services:

### Docker Compose Setup for PostgreSQL Cluster: Overview and Usage

##### Overview

This Docker Compose setup creates a scalable PostgreSQL cluster with one master node and two replica nodes. The configuration ensures horizontal scalability and data replication, enhancing data redundancy and read performance.

##### How It Works

1. **Master Node**:
    - Handles all read and write operations.
    - Configured with replication settings to send WAL logs to replica nodes.
    - Key settings: `wal_level=replica`, `max_wal_senders=3`, `wal_keep_segments=64`.

2. **Replica Nodes**:
    - Connect to the master node and replicate its data.
    - Use `primary_conninfo` to specify the connection details to the master node.
    - Follow the latest timeline of the master node with `recovery_target_timeline='latest'`.

3. **Access Control**:
    - `pg_hba.conf` is configured to allow local and network connections for replication.
    - Includes entries to permit connections from specific IP ranges.

4. **Data Persistence**:
    - Each node uses a specific volume to store its data, ensuring persistence across container restarts.
    - Configuration files are mapped to each container to customize PostgreSQL settings for each node.

#### Usage

- **Starting the Cluster**:
    - Run `docker-compose up -d` to start all services in detached mode. This sets up the PostgreSQL nodes, creates networks, and starts the containers.

- **Interaction and Management**:
    - Containers use service names (`db_node1`, `db_node2`, `db_node3`) to interact, allowing replica nodes to connect to the master node.
    - View running containers with `docker-compose ps`.
    - Monitor logs with `docker-compose logs`.
    - Stop the cluster with `docker-compose down`, which stops and removes containers while preserving data in volumes.

### Scalability and Replication

This setup focuses on horizontal scalability by allowing additional replica nodes to be added easily. The master node continuously sends data changes to the replica nodes, ensuring they have up-to-date copies of the data. This improves read performance and provides high availability, as read operations can be distributed across multiple nodes.

By leveraging Docker Compose, the setup and management of a scalable PostgreSQL cluster with data replication are simplified, enabling efficient handling of increased loads and enhanced fault tolerance.

## Step 4: Start the Services

Run Docker Compose to start the containers:

```bash
docker-compose up -d
```

![Running Containers](./images/docker-compose.png)

## Step 5: Create the Table on the Master Node

Create the `test_replication` table on the master node:

![Create Table](./images/create.png)

## Step 6: Insert Data on the Replica Node

Insert data on replica node 1 (`db_node2`):

![Insert Data](./images/insert.png)

## Step 7: Verify Data on the Master Node

Verify that the data has been replicated to the master node (`db_node1`):

![Select Data](./images/view.png)

## Conclusion

You have successfully set up a PostgreSQL cluster with replication using Docker Compose. The data inserted into a replica node is correctly replicated to the master node, demonstrating a functional replication setup in PostgreSQL.

For any issues or questions, check the configuration files and Docker logs for more details.


Make sure to capture the appropriate screenshots and place them in the `images/` directory, then update the README with the correct file paths to these images.