# Step 1: Create and Launch the Docker Container

1. **Launch the Docker container**:
    - Open a terminal and navigate to the directory where your `docker-compose.yml` file is located.
    - Run the following command to start the container:

      ```sh
      docker-compose up -d
      ```

3. **Verify the container is running**:
    - Run the following command to check that the container is up and running:

      ```sh
      docker ps
      ```

    - You should see a list of running containers, including `my_SocialDB`.

1. **Connect to the PostgreSQL container**:
    - Open a terminal and run the following command to connect to the running PostgreSQL container:

      ```sh
       psql -h localhost -p 5432 -U my_user -d postgres
      ```

2. **Verify the connection**:
    - You should now be connected to the PostgreSQL database and see a prompt like this:

      ```sh
      psql (12.4)
      Type "help" for help.
 
      my_SocialDB=#
      ```

    - You can now execute SQL commands directly in the PostgreSQL database.

![Connecting to PostgreSQL](images/connect_to_postgres.png)


# Step 2: Create the Tables

### the folder for tables are Practice1/tables

1. **Create the `users` table**:
   - Create the `users` table with list partitioning based on the initial character of the username. This allows for efficient querying and data management based on alphabetical ranges.

2. **Create the `posts` table**:
   - Create the `posts` table with range partitioning based on the creation date. This helps in managing and querying posts based on time periods, such as months or years.

3. **Create the `comments` table**:
   - Create the `comments` table with hash partitioning based on the `post_id`. Hash partitioning distributes the data evenly across partitions, ensuring balanced storage and access performance.

# Step 3: Create the Table Partitions

### the folder is Practice1/partitions

1. **Create the `users` table partitions `listPartitioning`**:
   - Order and Reason**: The `users` table is partitioned first because it serves as a primary reference for other tables such as `posts` and `comments`. Partitioning the `users` table by the initial character of the username helps in organizing and quickly accessing user data.
   - Partitioning Criteria**: The table is partitioned by the first character of the username (`username_initial`). This list partitioning allows for efficient querying and data management based on alphabetical ranges.

2. **Create the `posts` table partitions `rangePartitioning`** :
   - Order and Reason**: The `posts` table is partitioned after the `users` table because it references `users`. Partitioning `posts` based on the creation date helps manage data chronologically, making it easier to handle and query posts within specific time frames.
   - **Partitioning Criteria**: The table is partitioned by range based on the `created_at` timestamp. Range partitioning allows for efficient querying and archiving of data by specific time periods, such as months or years.

3. **Create the `comments` table partitions `hasPartitioning`**:
   - Order and Reason**: The `comments` table is partitioned last because it references both `users` and `posts`. Hash partitioning ensures that comments are evenly distributed, avoiding hotspots and ensuring balanced performance.
   - Partitioning Criteria**: The table is partitioned by hash based on the `post_id`. Hash partitioning distributes the data evenly across partitions, which helps in maintaining balanced storage and access performance, especially useful for large volumes of data.

Creating the partitions in this order ensures referential integrity and optimal performance. Each partitioning strategy is chosen based on the specific access patterns and organizational needs for the data in each table.

# Step 4: Create Functions to Insert Data into the Tables

In this step, we create functions to insert data into the `users`, `posts`, and `comments` tables. These functions automate the data insertion process, ensuring that data is distributed correctly across the partitions.

1. **Function to Insert Data into the `users` Table**:
   - Functionality**: This function generates and inserts multiple user records into the `users` table. It determines the initial character of the username (`username_initial`) to ensure that each user is placed in the correct partition.
   - Technical Explanation**: The function loops through a range of values to create usernames and emails. It calculates the `username_initial` based on the username, which is used to determine the appropriate partition. The function then inserts the generated data into the `users` table.

2. **Function to Insert Data into the `posts` Table**:
   - Functionality**: This function inserts posts into the `posts` table, associating each post with a user. It also assigns a creation date to each post to ensure correct placement in the date-based partitions.
   - Technical Explanation**: The function generates content for each post and assigns it to a user by referencing the `user_id`. It calculates the creation date to determine the appropriate range partition. The function then inserts the post data, including the `username_initial` to maintain referential integrity with the `users` table.

3. **Function to Insert Data into the `comments` Table**:
   - Functionality**: This function inserts comments into the `comments` table, associating each comment with a specific post and user. It uses hash partitioning to distribute comments evenly.
   - Technical Explanation**: The function generates content for each comment and associates it with a post by referencing the `post_id`. It ensures that each comment is linked to a user by including the `user_id` and `username_initial`. The hash partitioning strategy ensures that comments are evenly distributed across the available partitions.

These functions streamline the process of populating the database with test data, ensuring that the partitioning strategies are respected and that data is efficiently organized and accessible.

# Step 5: Execute the Insert Functions

In this step, we execute the functions created in Step 4 to insert data into the `users`, `posts`, and `comments` tables. This will populate the database with test data and ensure that the partitioning strategies are working as intended.

1. **Execute the Function to Insert Data into the `users` Table**:
   - Run the following command to execute the function that inserts data into the `users` table:

     ```sh
     SELECT insert_users();
     SELECT * FROM users;
     ```

   - This function will generate and insert multiple user records into the appropriate partitions based on the initial character of the username.

   ![Insert Users](images/insert_users.png)

2. **Execute the Function to Insert Data into the `posts` Table**:
   - Run the following command to execute the function that inserts data into the `posts` table:

     ```sh
     SELECT insert_posts();
     SELECT * FROM posts;
     ```

   - This function will generate and insert posts, associating each post with a user and placing it in the correct partition based on the creation date.

   ![Insert Posts](images/insert_posts.png)

3. **Execute the Function to Insert Data into the `comments` Table**:
   - Run the following command to execute the function that inserts data into the `comments` table:

     ```sh
     SELECT insert_comments();
     SELECT * FROM comments;
     ```

   - This function will generate and insert comments, associating each comment with a post and user, and ensuring even distribution across partitions.

   ![Insert Comments](images/insert_comments.png)

By executing these functions, the database is populated with data that adheres to the defined partitioning strategies, ensuring efficient organization and access.

# Step 6: View the Partitions

In this final step, we verify that the data has been correctly inserted into the appropriate partitions. We will run queries to select data from each partition and check the results. This ensures that our partitioning strategy is working as intended.

1. **View the `users` Table Partitions**:
   - Run the following query to view data in the `users_a_to_m` partition:

     ```sh
     SELECT * FROM users_a_to_m;
     ```

   - This partition contains users whose `username_initial` falls within 'a' to 'm'.

   ![Users A to M](images/view_users_a_to_m.png)

   - Run the following query to view data in the `users_n_to_z` partition:

     ```sh
     SELECT * FROM users_n_to_z;
     ```

   - This partition contains users whose `username_initial` falls within 'n' to 'z'.

   ![Users N to Z](images/view_users_n_to_z.png)

2. **View the `posts` Table Partitions**:
   - Run the following query to view data in the `posts_2023` partition:

     ```sh
     SELECT * FROM posts_2023;
     ```

   - This partition contains posts created in the year 2023.

   ![Posts 2023](images/view_posts_2023.png)

   - Run the following query to view data in the `posts_2024` partition:

     ```sh
     SELECT * FROM posts_2024;
     ```

   - This partition contains posts created in the year 2024.

   ![Posts 2024](images/view_posts_2024.png)

3. **View the `comments` Table Partitions**:
   - Run the following query to view data in the `comments_part_1` partition:

     ```sh
     SELECT * FROM comments_part_1;
     ```

   - This partition contains comments that fall under hash key 0.

   ![Comments Part 1](images/view_comments_part_1.png)

   - Run the following query to view data in the `comments_part_2` partition:

     ```sh
     SELECT * FROM comments_part_2;
     ```

   - This partition contains comments that fall under hash key 1.

   ![Comments Part 2](images/view_comments_part_2.png)

By running these queries, you can verify that the data has been correctly inserted into their respective partitions. The images help illustrate the results you should expect from each query, confirming the effectiveness of the partitioning strategy.
