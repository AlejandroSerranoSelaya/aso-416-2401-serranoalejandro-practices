CREATE OR REPLACE FUNCTION insert_users()
    RETURNS void AS $$
DECLARE
    i INT;
    username_prefix CHAR(1);
    email TEXT;
BEGIN
    FOR i IN 1..20 LOOP
            IF i % 2 = 0 THEN
                username_prefix := CHR(65 + (i % 13)); 
            ELSE
                username_prefix := CHR(78 + (i % 13)); 
            END IF;
            email := 'user' || i || '@example.com';
            INSERT INTO users (username, email, username_initial) VALUES (LOWER(username_prefix) || '_user' || i, email, LOWER(username_prefix));
        END LOOP;
END;
$$ LANGUAGE plpgsql;

-- Insertar posts
CREATE OR REPLACE FUNCTION insert_posts()
    RETURNS void AS $$
DECLARE
    i INT;
    u_id INT;
    u_initial CHAR(1);
    content TEXT;
    post_date DATE;
BEGIN
    FOR i IN 1..20 LOOP
            u_id := (i % 10) + 1;
            SELECT username_initial INTO u_initial FROM users WHERE user_id = u_id;
            content := 'This is post number ' || i;
            IF i % 2 = 0 THEN
                post_date := '2023-01-01'::DATE + (i * 5);
            ELSE
                post_date := '2024-01-01'::DATE + ((i - 10) * 5);
            END IF;
            INSERT INTO posts (user_id, content, created_at, username_initial) VALUES (u_id, content, post_date, u_initial);
        END LOOP;
END;
$$ LANGUAGE plpgsql;

-- Insertar comentarios
CREATE OR REPLACE FUNCTION insert_comments()
    RETURNS void AS $$
DECLARE
    i INT;
    post_id_val INT;
    user_id_val INT;
    u_initial CHAR(1);
    content TEXT;
    comment_date TIMESTAMP; 
BEGIN
    FOR i IN 1..20 LOOP
            IF i % 2 = 0 THEN
                post_id_val := (i % 10) + 1; 
            ELSE
                post_id_val := (i % 10) + 11; 
            END IF;

            user_id_val := (i % 10) + 1;

            SELECT username_initial INTO u_initial
            FROM users
            WHERE user_id = user_id_val;

            content := 'This is comment number ' || i;
            SELECT created_at INTO comment_date
            FROM posts
            WHERE post_id = post_id_val;

            INSERT INTO comments (post_id, user_id, content, created_at, username_initial)
            VALUES (post_id_val, user_id_val, content, comment_date, u_initial);
        END LOOP;
END;
$$ LANGUAGE plpgsql;