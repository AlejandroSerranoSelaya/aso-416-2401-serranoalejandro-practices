CREATE TABLE users_a_to_m PARTITION OF users
    FOR VALUES IN ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm');

CREATE TABLE users_n_to_z PARTITION OF users
    FOR VALUES IN ('n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');