CREATE TABLE posts (
    post_id SERIAL,
    user_id INT,
    username_initial CHAR(1) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (post_id, created_at),
    FOREIGN KEY (user_id, username_initial) REFERENCES users (user_id, username_initial)
) PARTITION BY RANGE (created_at);
